import React from 'react';
import AppNavBar from './components/AppNavBar';
import ListeCourse from './components/ListeCourse';
import ItemModal from './components/ItemModal';
import {Container} from "reactstrap";

import {Provider} from 'react-redux';
import store from './store';




import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';



function App() {
  return (
     <Provider store={store}>
    <div className="App">
      <AppNavBar />
      <Container>
      <ItemModal />
      <ListeCourse/>
      </Container>
    </div>
    </Provider>
  );
}

export default App;

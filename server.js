const express = require('express');
const mongoose = require('mongoose');
const path = require('path');

const items = require('./routes/api/items');

const app = express();

//Format Json
app.use(express.json());

//Connexion à MongoDB
const db = require('./config/keys.js').mongoURI;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

mongoose.connect(db)
   .then(()=> console.log("Connexion faite !"))
   .catch(err => console.log(err));

// Utilisation des routes
app.use("/api/items", items);

//Ressources statiques si mode production
if (process.env.NODE_ENV === 'production') {
   // Choisir dossier statique
   app.use(express.static("client/build"));
   app.get("*", (req, res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
   });
}

const port = process.env.PORT || 5000;
app.listen(port, ()=> console.log(`Serveur lancé sur le port ${port}`));